#!/bin/bash

DATE=`date +%Y-%m-%d-%H-%M`
DIRECTORY_OUT=/home/student/c++
BACKUP_DIRECTORY=/home/student/backup_c++
GROUP=group_a


#DO not edit below this line
REPO_NAME=https://bitbucket.org/nokia-krakow

#For dubug purpose only. Leave it as zero. It can be changed via command line (-d debbug_level)
DEBUG=0

if [ $# -eq 0 ]
then
	echo "run \"$0 --help\"  to see script option"
  exit
fi

function usage ()
{
    echo ""
    echo "USAGE: "
    echo "    $0 [-?d]"
    echo ""
    echo "OPTIONS:"
    echo "    -d  debug level (0-5)"
    echo "    -g  group"
    echo "    -?  this usage information"
    echo ""
    echo "EXAMPLE:"
    echo "    $0 -d 1 -r "
    echo ""
    echo "option -r  repository and -g  group ARE MANDATORY!!!"
    exit $E_OPTERROR    # Exit and explain usage, if no argument(s) given.
}

function processArgs(){
  while getopts ":d:r:g:?" Option
  do
      case $Option in
          d    ) DEBUG=$OPTARG;;
          g    ) GROUP=$OPTARG;;
          ?    ) usage
                 exit 0;;
          *    ) echo ""
                 echo "Unimplemented option chosen."
                 usage   # DEFAULT
      esac
  done
  shift $(($OPTIND - 1))
}


processArgs "$@"


trap 'previous_command=$this_command; this_command=$BASH_COMMAND' DEBUG


#Auxiliary function
function checkStatus()
{
  if [ $DEBUG -gt 0 ]
  then 
    lastCommand=$previous_command
    if [ $1 -ne 0 ] 
    then
      echo -e "\e[31mFAILURE"	
      echo "*****************************"
      echo "The above ERROR is due to  the failure of the following command"
      echo $lastCommand
      echo -e "\e[0m"
    else
      echo "*****************************"
      echo "success"
      echo $lastCommand
      echo "*****************************"
    fi
  fi
}


if [ ! -d $BACKUP_DIRECTORY ]
then
  mkdir $BACKUP_DIRECTORY 
  checkStatus $?
fi

if [ -d $DIRECTORY_OUT ]
then
  mv $DIRECTORY_OUT ${BACKUP_DIRECTORY2}/${DATE}
  checkStatus $?
fi

mkdir $DIRECTORY_OUT 
checkStatus $?
cd $DIRECTORY_OUT
#Clone the given repository
git clone ${REPO_NAME}/${GROUP}
echo ${REPO_NAME}/${GROUP}

checkStatus $?
cd $GROUP
#cmake .
#make
#checkStatus $?
echo ""
echo -e "\e[32m*****************************"
echo "Created a new local repository for ${GROUP} in: "
echo "$DIRECTORY_OUT"
echo "Previous user repository has been moved to:"
echo "${BACKUP_DIRECTORY}/${DATE}"
echo "*****************************"
echo -e "\e[0m"
qtcreator CMakeLists.txt
